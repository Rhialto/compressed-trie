# Makefile for the trie demo.

all: maketrie readtrie

CFLAGS += -Wall -ggdb

maketrie: maketrie.o
	$(CC) $(CFLAGS) maketrie.o -o maketrie

readtrie: readtrie.o
	$(CC) $(CFLAGS) readtrie.o -o readtrie

maketrie.o: trie.h
readtrie.o: trie.h
