/*
 * maketrie.c
 * Copyright 2005 Olaf Seibert.
 *
 * Store data in a standard trie data structure.
 * Inspired by Knuth. Optimised sibling storage is of my own invention.
 * Compressed addition inspired by "A Trie Compaction Algorithm for a Large
 * Set of Keys" by Jun-ichi Aoe et al., 1996
 * (but without a separate RE trie, the rear trie simply starts where
 * refcount > 1).
 */

#include <stdlib.h>
#include <stdio.h>
#include <string.h>
#include <errno.h>
#include <ctype.h>
#include <assert.h>

#include "trie.h"

#define DEBUG			0
#define USE_COMPRESSED_ADDITION	1

typedef int Data;

typedef struct node {
    TriePos 	child;
    TriePos	sibling;
    Char	character;
    int		refcount;
    Data	data;
} node;

/*
 * These values can be tuned:
 */
#define INITIAL_TRIESIZE	10000
#define DUPLICATE_SEARCH_DIST	 1024
#define COMPRESSION_TIMES	    1
#define DUPLICATE_SEARCH_HASH	 1024

#define HISTOGRAM		    1
#define HASH_HISTOGRAM		    1
#define EXTRA_HISTOGRAM		    0

static int trie_size;
static node *trie;
static int trie_next_free, trie_avail, trie_nodes_in_use;
static TriePos trie_root;
static TriePos trie_freelist;

/* For the compression: */
static TriePos *trie_hash;
static int trie_nodes_freed;
static int trie_shared_siblings;

/* For the binary dump: */
static FILE *bindump;

/* For statistics only: */
static int trie_shared_children, trie_unshared_children;
static int trie_unshared_siblings;
static int trie_input_size, trie_words;

static int nodes_put_on_freelist, nodes_taken_from_freelist;

#if HISTOGRAM
static int search_back_distance[DUPLICATE_SEARCH_DIST];
#endif
#if EXTRA_HISTOGRAM
static int could_have_found;
#endif
#if DUPLICATE_SEARCH_HASH
static int duplicate_search_hash[DUPLICATE_SEARCH_HASH];
#if HASH_HISTOGRAM
static int duplicate_search_hash_usage[DUPLICATE_SEARCH_HASH];
#endif	/* HASH_HISTOGRAM */
#endif	/* DUPLICATE_SEARCH_HASH */

void pretty_print_trie(void);
int trie_compress(void);

//#define HASH(chr, child, sibling)	(3*(chr) + 7*(child) + 11*(sibling))
#define HASH(chr, child, sibling)	(7*(chr) + 11*(child) + 13*(sibling))

/*
 * Create a new node with given character, child and sibling.
 * Increases the refcounts of child and sibling,
 * and of itself (to 1) on the assumption that this node will
 * be referenced.
 */
static
TriePos
trie_new_node(Char character, TriePos child, TriePos sibling)
{
    TriePos here = -1;

    if (character == CHAR_NULLPTR || character == CHAR_SHAREPTR) {
	fprintf(stderr, "trying to store forbidden character %d\n", character);
	exit(1);
    }

#if USE_COMPRESSED_ADDITION
    if (trie_freelist) {
	here = trie_freelist;
	trie_freelist = trie[trie_freelist].child;
	nodes_taken_from_freelist++;
#if DEBUG
	fprintf(stderr, "taken %d/%d from freelist, %d is next\n",
		here,
		trie[here].refcount,
		trie_freelist);
#endif
	if (trie[here].refcount != 0) {
	    fprintf(stderr, "XXX refcount %d!\n", trie[here].refcount);
	}
    } else
#endif
    if (trie_avail > 0) {
	here = trie_next_free++;
	trie_avail--;
    }

    if (here >= 0) {
	trie[here].character = character;
	trie[here].child = child;
	trie[here].sibling = sibling;
	trie[here].refcount = 1;
	trie[here].data = 0;

	if (character && child)
	    trie[child].refcount++;
	if (sibling)
	    trie[sibling].refcount++;

	trie_nodes_in_use++;

	return here;
    }
    fprintf(stderr, "Out of trie nodes!\n");
    exit(1);
}

int
trie_init(int size)
{
    trie = malloc(size * sizeof (node));
    if (trie != NULL) {
	trie_size = size;
	trie_avail = trie_size;
	trie_next_free = 0;
	trie_nodes_in_use = 0;

	trie_root = trie_new_node('^', 0, 0);

	return 0;
    }
    return ENOMEM;
} 

/*
 * Make sure 'needed' nodes are free to be allocated.
 * If necessary, compress the trie and/or allocate more memory.
 * This should only be called in between inserting words, since
 * it may move the trie nodes around; any pointer to any one of
 * them may become invalid (in particular the *prev pointer).
 */
int
trie_nodes_available(int needed)
{
    /*
     * When running out of free nodes, alternate between compressing
     * the trie, and allocating new memory.
     * If you compress more often, you save more memory but it will
     * cost more time.
     */
    static int compression_times = 0;
    /*
     * There is no point in compressing more than once with no trie
     * modification in between.
     */
    int compressed_this_time = 0;

    while (trie_nodes_in_use + needed >= trie_size) {
	if (compression_times < COMPRESSION_TIMES &&
		!compressed_this_time) {
#if USE_COMPRESSED_ADDITION
	    trie_compress();
#endif
	    compression_times++;
	    compressed_this_time = 1;
	} else {
	    int increment = trie_size;
	    int newsize = trie_size + increment;
	    node *newtrie;

	    compression_times = 0;

	    fprintf(stderr, "Growing trie: %d -> %d nodes\n", trie_size, newsize);
	    newtrie = realloc(trie, sizeof(node) * newsize);

	    if (newtrie) {
		trie = newtrie;
		trie_size = newsize;
		trie_avail += increment;
	    } else {
		return 0;
	    }
	}
    }

    return 1;
}

/*
 * Add the word as a child to the given location in the trie.
 * Return the location of the end-marker.
 */
static
TriePos
trie_add_here(TriePos here, Char *word)
{
    TriePos *prev = &trie[here].child;
    TriePos child = *prev;

    if (!child) {
	/* If there is no child yet, we need to create one */
	*prev = child = trie_new_node(word[0], 0, 0);
    } else {
	/*
	 * There is at least one child, find where which one corresponds
	 * to our word, or otherwise find the location to insert a new one.
	 * We keep the sibling list sorted by character, therefore
	 * tc = trie[child].character will be increasing through the loop.
	 */
	Char tc;
	Char c = word[0];

	while (child != 0 && (tc = trie[child].character, tc < c)) {
	    prev = &trie[child].sibling;
	    child = *prev;
	}
	if (tc == c) {
	    /* no need to do anything */
	} else {
	    /* insert our new child between 'prev' and 'child' */
	    *prev = child = trie_new_node(word[0], 0, child);
	}
    }

    /*
     * recursively add the rest of the word
     */
    if (word[0]) {
	return trie_add_here(child, &word[1]);
    }
    return child;
}

TriePos
trie_add(Char *word)
{
    return trie_add_here(trie_root, word);
}

/*
 * Functions for adding a word to a trie that may be partially
 * compressed, and do it in a compressed way if possible.
 */

/*
 * Find a node given character child and sibling.
 * If found, increase refcount and return it.
 * Otherwise make a new one (also with refcount already set).
 */
TriePos
find_or_make_node(Char character, TriePos child, TriePos sibling)
{
    TriePos here, limit;
    int hashval;
#if HISTOGRAM
    int startpos;
#endif

#if DUPLICATE_SEARCH_HASH
    /*
     * If we don't find the desired node by the hash lookup (which is
     * quite likely), we go on to search for it anyway, we may get
     * lucky. Starting at the hightest occupied node doesn't really
     * make sense anyway, since very often nodes will be pulled from
     * the freelist and therefore recently allocated nodes are scattered
     * all over the place.
     */
    hashval = HASH(character, child, sibling) % DUPLICATE_SEARCH_HASH;
    here = duplicate_search_hash[hashval];
    if (trie[here].character == CHAR_SHAREPTR) {
	here = trie[here].sibling;
    }
    if (!here)
#endif
	here = trie_next_free - 1;
#if HISTOGRAM
    startpos = here;
#endif
    
    /*
     * Search backwards from the nodes that we have to find
     * a candidate.
     */
    limit = trie_next_free - DUPLICATE_SEARCH_DIST;
    if (limit < 0)
	limit = 0;

    for (; here >= limit; here--) {
	if (trie[here].character == character &&
	    trie[here].child == child &&
	    trie[here].sibling == sibling) {
#if DEBUG
	    fprintf(stderr, "found a duplicate: %c %d ->%d v%d refc %d\n",
		    character, here, child, sibling, trie[here].refcount);
#endif
	    trie[here].refcount++;
#if HISTOGRAM
	    search_back_distance[startpos - here]++;
#endif
	    return here;
	}
    }
#if HISTOGRAM
    search_back_distance[DUPLICATE_SEARCH_DIST-1]++;	/* not found */
#endif
#if EXTRA_HISTOGRAM
    /*
     * Take (a lot!) of extra time to check if we *could* have found
     * a duplicate node if we had a perfect algorithm to find it.
     */
    for (here = trie_next_free - 1; here > 0; here--) {
	if (trie[here].character == character &&
	    trie[here].child == child &&
	    trie[here].sibling == sibling) {
#if DEBUG
	    fprintf(stderr, "missed  duplicate: %c %d ->%d v%d refc %d \n",
		    character, here, child, sibling, trie[here].refcount);
#endif
	    could_have_found++;
	    break;
	}
    }
#endif

    here = trie_new_node(character, child, sibling);
#if DUPLICATE_SEARCH_HASH
#if HASH_HISTOGRAM
    if (duplicate_search_hash[hashval] != here)
	duplicate_search_hash_usage[hashval]++;
#endif	/* HASH_HISTOGRAM */
    duplicate_search_hash[hashval] = here;
#endif	/* DUPLICATE_SEARCH_HASH */
    return here;
}

TriePos
make_node_with_data(Char character, TriePos child, TriePos sibling, Data data)
{
    TriePos here;
    here = trie_new_node(character, child, sibling);
    trie[here].data = data;
    return here;
}

/*
 * Create nodes for a word, backwards, so we can optimally share nodes.
 * child and sibling are the values that must be set at the \0 node.
 */

TriePos
find_or_make_node_chain(Char *word, TriePos child)
{
    if (word[0]) {
	child = find_or_make_node_chain(word + 1, child);
	/*
	 * Need to decrease refcount since creating makes it 1 and
	 * linking it into the tie increases it, but it should stay 1.
	 */
	if (word[0] && child)
	    trie[child].refcount--;
    }
    return find_or_make_node(word[0], child, 0);
}

TriePos
find_or_make_node_chain_with_data(Char *word, TriePos child, Data data)
{
    TriePos child1, child2;

    /*
     * Make the unshared node that holds the data
     */
    child1 = make_node_with_data(word[0], child, 0, data);
    /*
     * Make the rest of the word, this may be shared
     */
    child2 = find_or_make_node_chain(word + 1, child);
    trie[child1].child = child2;

    return child1;
}

/*
 * Add the word as a child to the given location in the trie.
 * DOES NOT (YET) return the location of the end-marker.
 */
static
TriePos
trie_add_compressed_here(TriePos here, Char *word)
{
    TriePos *prev = &trie[here].child;
    TriePos child = *prev;
#if DEBUG
    int prevref = trie[here].refcount;
#endif

    if (!child) {
	/* If there is no child yet, we need to create one */
	assert(trie[here].refcount == 1);
	*prev = child = find_or_make_node_chain(word, 0);
    } else {
	/*
	 * There is at least one child, find where which one corresponds
	 * to our word, or otherwise find the location to insert a new one.
	 * We keep the sibling list sorted by character, therefore
	 * tc = trie[child].character will be increasing through the loop.
	 */
	Char tc = -1;
	Char c = word[0];

	for (;;) {
	    if (child == 0)
		break;
	    /*
	     * Unshare (i.e. copy) if needed, since we're going to
	     * change some data down the line eventually.
	     * This corresponds to transfering nodes from the RE to the FR
	     * trie in Aoe et al..
	     */
	    if (trie[child].refcount > 1) {
		TriePos newchild, tsibling, tchild;
		Char tcharacter;

		tcharacter = trie[child].character;
		tsibling   = trie[child].sibling;
		tchild     = trie[child].child;

		/*
		 * This will increase the refcount on tchild and tsibling,
		 * so whichever we visit next, it will be unshared too.
		 * The new copy will have refcount == 1.
		 */
		newchild = trie_new_node(tcharacter, tchild, tsibling);
#if DEBUG
		fprintf(stderr, "unsharing %d to copy %d\n", child, newchild);
#endif

		/* fix refcount on original node */
		trie[child].refcount--;
		/*
		 * Invariant: we may modify the previous/parent node,
		 * because its refcount == 1.
		 */
		*prev = child = newchild;
#if DEBUG >= 3
		pretty_print_trie();
		assert(prevref == 1);
#endif
	    }
	    tc = trie[child].character;
	    if (tc >= c)
		break;
#if DEBUG
	    prevref = trie[child].refcount;
#endif
	    prev = &trie[child].sibling;
	    child = *prev;
	}
	if (tc == c) {
	    /* no need to do anything */
	} else {
	    /* insert our new child between 'prev' and 'child' */
	    if (child)
		trie[child].refcount--;
	    *prev = child = find_or_make_node(word[0], 0, child);
	}

	/*
	 * recursively add the rest of the word
	 */
	if (word[0]) {
	    return trie_add_compressed_here(child, &word[1]);
	}
    }

    return child;
}

TriePos
trie_add_compressed(Char *word)
{
    return trie_add_compressed_here(trie_root, word);
}

void
move_data_from_to(TriePos from, TriePos to)
{
    assert(trie[to].data == 0);
    trie[to].data = trie[from].data;
    trie[from].data = 0;
}

/*
 * Add the word and the data as a child to the given location in the trie.
 */
static
TriePos
trie_add_compressed_with_data_here(TriePos here, Char *word, Data data)
{
    TriePos *prev = &trie[here].child;
    TriePos child = *prev;
#if DEBUG
    int prevref = trie[here].refcount;
#endif

    if (!child) {
	/* If there is no child yet, we need to create one */
	assert(trie[here].refcount == 1);
	*prev = child = find_or_make_node_chain_with_data(word, 0, data);
    } else {
	/*
	 * There is at least one child, find where which one corresponds
	 * to our word, or otherwise find the location to insert a new one.
	 * We keep the sibling list sorted by character, therefore
	 * tc = trie[child].character will be increasing through the loop.
	 */
	Char tc = -1;
	Char c = word[0];
	int unshared = 0;

	for (;;) {
	    if (child == 0)
		break;
	    /*
	     * Unshare (i.e. copy) if needed, since we're going to
	     * change some data down the line eventually.
	     * This corresponds to transfering nodes from the RE to the FR
	     * trie in Aoe et al..
	     */
	    if (trie[child].refcount > 1) {
		TriePos newchild, tsibling, tchild;
		Char tcharacter;

		tcharacter = trie[child].character;
		tsibling   = trie[child].sibling;
		tchild     = trie[child].child;

		/*
		 * This will increase the refcount on tchild and tsibling,
		 * so whichever we visit next, it will be unshared too.
		 * The new copy will have refcount == 1.
		 */
		newchild = trie_new_node(tcharacter, tchild, tsibling);
#if DEBUG
		fprintf(stderr, "unsharing %d to copy %d\n", child, newchild);
#endif

		/* fix refcount on original node */
		trie[child].refcount--;
		/*
		 * Invariant: we may modify the previous/parent node,
		 * because its refcount == 1.
		 */
		*prev = child = newchild;
#if DEBUG >= 3
		pretty_print_trie();
		assert(prevref == 1);
#endif
		unshared = 1;
	    }
	    tc = trie[child].character;
	    if (tc >= c)
		break;
#if DEBUG
	    prevref = trie[child].refcount;
#endif
	    prev = &trie[child].sibling;
	    child = *prev;
	}
	if (unshared) {
	    /*
	     * Now that we expanded the FRont trie a bit to the right, 
	     * the private node is no longer private, so we need to move
	     * its data.
	     * assume that the parent has only one child, and that is
	     * the one to move the data to.
	     * ADDED: apparently the "only one child" assumption is both
	     *        not true and not needed.
	     * We want to refer to the parent, 
	     * because it is the one that still has refcount == 1
	     */
	    //fprintf(stderr, "here = %d, child = %d\n", here, child);
	    TriePos thechild = trie[here].child;
	    //fprintf(stderr, "thechild = %d, its sibling = %d\n", thechild, trie[thechild].sibling);
#if DEBUG || 1
	    if (trie[thechild].sibling) {
		pretty_print_trie();
		/* assert(! trie[thechild].sibling); */
	    }
#endif
	    move_data_from_to(here, thechild);
	}
	if (tc == c) {
	    /* no need to do anything */
	} else {
	    /* insert our new child between 'prev' and 'child' */
	    if (child)
		trie[child].refcount--;
	    /*
	     * We are making a new node for this new word, so put
	     * its data there.
	     * For the rest, we don't need to concern ourselves
	     * with it.
	     */
	    *prev = child = make_node_with_data(word[0], 0, child, data);
	    if (word[0]) {
		return trie_add_compressed_here(child, &word[1]);
	    }
	    return child;
	}

	/*
	 * recursively add the rest of the word
	 */
	if (word[0]) {
	    return trie_add_compressed_with_data_here(child, &word[1], data);
	}
    }

    return child;
}

TriePos
trie_add_compressed_with_data(Char *word, Data data)
{
    return trie_add_compressed_with_data_here(trie_root, word, data);
}

/*
 * BUG: if USE_COMPRESSED_ADDITION
 * the first letter of the first word to be added to the trie
 * is duplicated!
 */

int
trie_fill(FILE *f)
{
    char line[LINESIZE];
    int size = 0;
    int words = 0;

    while (fgets(line, LINESIZE, f)) {
	int len = strlen(line);
	if (len > 0 && line[len-1] == '\n')
	    line[--len] = '\0';
	size += len + 1;	/* count \0 terminator too */
	trie_input_size = size;
	words++;
	trie_nodes_available(len + 1);
#if DEBUG
	fprintf(stderr, "adding '%s'\n", line);
#endif
#if USE_COMPRESSED_ADDITION
	trie_add_compressed_with_data((Char *)line, words);	/* XXX needs conversion of the string */
#else
	trie_add((Char *)line);			/* XXX needs conversion of the string */
#endif
#if DEBUG >= 3
	pretty_print_trie();
#endif
    }

    fprintf(stderr, "trie_fill: %d nodes in use for %d words %d characters (%f)\n",
	    trie_next_free, words, size, (float) trie_next_free / size);

    trie_words = words;

    return size;
}

static
TriePos 
trie_find_equiv(TriePos here)
{
    unsigned long hashval, startingpoint;
    node *n;

    n = &trie[here];

    /* Find this node, or its equivalent, in the hash table */
    hashval = HASH(n->character, n->child, n->sibling) % trie_next_free;
    startingpoint = hashval;

    for (;;) {
	TriePos eq = trie_hash[hashval];
	node *eqp;

	if (eq == 0) {
	    /*
	     * This is the first node with that hash value:
	     * we have free space for our info.
	     */
	    trie_hash[hashval] = here;
	    return here;
	}

	/* check if we found an equivalence */
	eqp = &trie[eq];
	if (eqp->character == n->character &&
	    eqp->child     == n->child     &&
	    eqp->sibling   == n->sibling) {
	    return eq;
	}
	hashval++;
	/* wrap around (modulo array size) */
	if (hashval >= trie_next_free)
	    hashval = 0;
	/* if we looked at everything, we haven't found it */
	if (hashval == startingpoint) {
	    return here;
	}
    }
}

static
TriePos
trie_compress_here(TriePos here)
{
    TriePos equiv;

    /* Check for "no trie" case */
    if (here == 0)
	return here;

    /*
     * Then check if we removed this node before.
     * TODO If we visited here before but didn't remove it,
     * we could still return immediately, but right now 
     * we can't detect that (inefficient).
     */ 
    if (trie[here].character == CHAR_SHAREPTR) {
	equiv = trie[here].sibling;
    } else {
	/* First fix up any sharing for the nodes we point to */
	if (trie[here].character) {
	    trie[here].child = trie_compress_here(trie[here].child);
	}
	trie[here].sibling = trie_compress_here(trie[here].sibling);

	/* Then compress ourselves out of existence, if possible. */
	equiv = trie_find_equiv(here);
    }

    if (equiv != here) {
	    /*
	     * Update the refcount on the nodes.
	     * Caller must replace 1 reference to here with one to equiv.
	     */
	    trie[here].refcount--;
	    trie[equiv].refcount++;

	if (trie[here].character == CHAR_SHAREPTR) {
#if DEBUG
	    fprintf(stderr, "trie_compress_here: re-visit %d/%d (use %d/%d)\n",
		    here,  trie[here].refcount,
		    equiv, trie[equiv].refcount);
#endif
	} else {
#if DEBUG
	    fprintf(stderr, "trie_compress_here: free %d/%d (use %d/%d) ('%c' ->%d v%d)\n",
		    here,  trie[here].refcount,
		    equiv, trie[equiv].refcount,
		    trie[here].character, trie[here].child, trie[here].sibling
		    );
#endif
	    /* Adjust the refcounts of the child/sibling. */
	    if (trie[here].character && trie[here].child) {
		trie[trie[here].child].refcount--;
#if DEBUG
		fprintf(stderr, "  child: %d.refcount-- -> %d\n",
			trie[here].child, trie[trie[here].child].refcount);
#endif
	    }
	    if (trie[here].sibling) {
		trie[trie[here].sibling].refcount--;
#if DEBUG
		fprintf(stderr, "sibling: %d.refcount-- -> %d\n",
			trie[here].sibling, trie[trie[here].sibling].refcount);
#endif
	    }
	    /*
	     * Free the node here, it is not in use anymore,
	     * since it can be replaced by node equiv.
	     */
	    trie[here].character = CHAR_SHAREPTR;
	    trie[here].sibling = equiv;

	    /* link it in the freelist via the child pointer */
	    trie[here].child = trie_freelist;
	    trie_freelist = here;
	    nodes_put_on_freelist++;	/* statistics */
	    trie_nodes_in_use--;	/* necessary */
	    trie_nodes_freed++;		/* statistics */
	}
    }
    
    return equiv;
}

/*
 * Tail-compress the trie.
 */
int
trie_compress()
{
    int nodes_left;

    trie_hash = calloc(trie_next_free, sizeof(*trie_hash));
    trie_nodes_freed = 0;
    /* trie_root is typically 0 so compress its child instead */
    trie[trie_root].child = trie_compress_here(trie[trie_root].child);
    free(trie_hash);
    trie_hash = NULL;

    nodes_left = trie_nodes_in_use;

    fprintf(stderr, "trie_compress: %d nodes freed, %d left (%f)\n",
	    trie_nodes_freed, nodes_left,
	    (float)nodes_left / trie_next_free);
    fprintf(stderr, "Overall compressed size: %f\n",
	    (float)nodes_left / trie_input_size);

    return nodes_left;
}

static
void
dumpchar(int i)
{
    /* perhaps as UTF-8 */
    putc((i >>  0) & 0xFF, bindump);
}

static
void
dumpint(int i)
{
    putc((i >> 16) & 0xFF, bindump);
    putc((i >>  8) & 0xFF, bindump);
    putc((i >>  0) & 0xFF, bindump);
}

/*
 * 3 bytes is plenty for a trie position.
 * A trie with 2^24 = 16 mega-nodes is humongous; about 2 million is 
 * enough for English before compression.
 */
static
void
dumptriepos(int i)
{
    putc((i >> 16) & 0xFF, bindump);
    putc((i >>  8) & 0xFF, bindump);
    putc((i >>  0) & 0xFF, bindump);
}

/*
 * Dump the trie breadth-first.
 * Count the nodes as they are written, to keep track of their numbers
 * when they will be read back in.
 * This is needed because we need to indicate the sharing in the new
 * numbering.
 * Dumping the trie destroys it.
 * (otherwise use an extra array to record the renumbering)
 *
 * We dump the nodes breadth-first, which places the siblings in a row.
 * This allows us to eliminate virtually all sibling pointers!
 *
 * There are some special cases though:
 * 1. We need to indicate the end of the row of siblings somehow.
 *    My first idea was to store the length of the row in each index,
 *    but it is even simpler to store an end-bit in the child index of
 *    the node itself.
 * 2. If the index of (pointer to) the next sibling is a sharing pointer, 
 *    we sacrifice a node with character CHAR_SHAREPTR and store the
 *    sibling index in the child index.
 *    When dumping, we need to count this extra node.
 * 3. To take care of (2) we count the sibling-number. If we arrive at
 *    a previously dumped node and the sibling number is 0, it was via
 *    a child pointer that we got here, otherwise via a sibling pointer.
 */
TriePos
trie_dump_binary_here(TriePos here, TriePos renumbered, int nsib)
{
    if (here == 0) {
	dumpchar(CHAR_NULLPTR);
    } else {
	Char c = trie[here].character;
	TriePos sibling = trie[here].sibling;

	/* Did we dump this node before? */
	if (c == CHAR_SHAREPTR) {
	    /* Yes, dump its (renumbered) TriePos */
	    dumpchar(CHAR_SHAREPTR);
	    dumptriepos(sibling);
	    if (nsib > 0) {
		renumbered++;
		trie_shared_siblings++;		/* consistency check */
	    } else {
		trie_shared_children++;		/* stats */
	    }
	} else {
	    /*
	     * No, dump the node and record its (renumbered) TriePos
	     * in case we visit it again later.
	     */
	    dumpchar(c);
	    trie[here].character = CHAR_SHAREPTR;
	    trie[here].sibling = renumbered++;
	    if (nsib > 0) {	/* statistics */
		trie_unshared_siblings++;	/* stats */
	    } else {
		trie_unshared_children++;	/* stats */
	    }
	    renumbered = trie_dump_binary_here(sibling, renumbered, nsib+1);
	    if (c == CHAR_EOS) {
		/* an endmarker does not have a child */
	    } else {
		renumbered = trie_dump_binary_here(trie[here].child, renumbered, 0);
	    }
	}
    }

    return renumbered;
}

/*
 * Dump the trie to a binary file.
 * Dumping the trie destroys it beyond repair.
 */
void
trie_dump_binary(char *filename, int nodes_in_trie)
{
    int nodes_dumped;
    long int start_of_file;

    bindump = fopen(filename, "w");
    start_of_file = ftell(bindump);
    dumpint(nodes_in_trie);
    /* don't save the root, it would be node #0 */
    nodes_dumped = trie_dump_binary_here(trie[trie_root].child, 1, 0);
    /* now go back and write the correct number of nodes */
    fseek(bindump, start_of_file, SEEK_SET);
    dumpint(nodes_dumped);

    fclose(bindump);

    /* print a bunch of statistics */
    fprintf(stderr, "%d nodes in trie, %d nodes saved: ",
	    nodes_in_trie, nodes_dumped);
    if (nodes_in_trie + trie_shared_siblings == nodes_dumped) {
	fprintf(stderr, "ok (due to nodes for shared siblings)\n");
    } else {
	fprintf(stderr, "MISMATCH (expected %d)\n",
		nodes_in_trie + trie_shared_siblings);
    }
    fprintf(stderr, "%f cq %f nodes / word, %f chars / word\n",
	    (float)nodes_in_trie / trie_words,
	    (float)nodes_dumped / trie_words,
	    (float)trie_input_size / trie_words);
    fprintf(stderr, "%d peak node useage, %d allocated\n",
	    trie_next_free, trie_size);
    fprintf(stderr, "%d shared siblings, %d unshared siblings\n",
	    trie_shared_siblings, trie_unshared_siblings);
    fprintf(stderr, "%d shared children, %d unshared children\n",
	    trie_shared_children, trie_unshared_children);
    fprintf(stderr, "using %ld bytes\n",
	    (long)(nodes_dumped * (sizeof(Char) + sizeof(TriePos))));
    fprintf(stderr, "(would have used %ld bytes with explicit siblings)\n",
	    (long)(nodes_in_trie * (sizeof(Char) + 2 * sizeof(TriePos))));

    assert(nodes_in_trie + trie_shared_siblings == nodes_dumped);

    fprintf(stderr, "%d nodes put on freelist, %d taken off\n",
	    nodes_put_on_freelist, nodes_taken_from_freelist);
}

static Char accumulator[LINESIZE];

static
void
trie_dump_text_here(TriePos here, int depth)
{
    Char c;

    accumulator[depth] = c = trie[here].character;

    if (c == '\0') {
	printf("%s\n", (char *)&accumulator[1]);
    } else {
	TriePos child = trie[here].child;

	while (child != 0) {
	    trie_dump_text_here(child, depth + 1);
	    child = trie[child].sibling;
	}
    }
}

/*
 * Print all the words stored in the trie.
 */
void
trie_dump_text()
{
    trie_dump_text_here(trie_root, 0);
}

/*
 * Pretty-print the trie, clearly showing its structure.
 *
 *  ^ ->  A ->  A ->  a ->  n ->  i ->  $    
 *  0/1   1/1   6/1   5/1   4/1   3/1   2/5    
 *  0     1     0     0     0     0     0       
 *
 *  $ = character (end of word)
 *  2/ = node number
 *  /5 = reference count
 *  0  = data
 */

#define PRINTLINESIZE	1000
#define PRINTWIDTH	6

static char line1[PRINTLINESIZE];
static char line2[PRINTLINESIZE];
static char line3[PRINTLINESIZE];
static char line4[PRINTLINESIZE];

void
pretty_print_trie_here(TriePos here, int depth)
{
    if (trie[here].refcount >= 0) {
	Char character = trie[here].character;
	if (character) {
	    if (trie[here].child) {
		sprintf(line1 + PRINTWIDTH * depth, " %c -> ", trie[here].character);
	    } else {
		sprintf(line1 + PRINTWIDTH * depth, " %c    ", trie[here].character);
	    }
	} else {
	    sprintf(line1 + PRINTWIDTH * depth, " $    ");
	}

	sprintf(line2 + PRINTWIDTH * depth, "%d/%d    ", here, trie[here].refcount);
	sprintf(line3 + PRINTWIDTH * depth, "%d       ", trie[here].data);

	if (trie[here].sibling) {
	    sprintf(line4 + PRINTWIDTH * depth, " |    ");
	} else {
	    sprintf(line4 + PRINTWIDTH * depth, "      ");
	}

	if (!character) {
	    printf("%s\n%s\n%s\n%s\n", line1, line2, line3, line4);
	}

	trie[here].refcount = -trie[here].refcount;

	if (character && trie[here].child) {
	    pretty_print_trie_here(trie[here].child, depth + 1);
	}
	if (trie[here].sibling) {
	    /* get rid of all parent details except | */
	    strcpy(line1, line4);
	    strcpy(line2, line4);
	    strcpy(line3, line4);
	    pretty_print_trie_here(trie[here].sibling, depth);
	}

    } else {
	sprintf(line1 + PRINTWIDTH * depth, "(%d)", here);
	sprintf(line2 + PRINTWIDTH * depth, "    ");
	sprintf(line3 + PRINTWIDTH * depth, "    ");
	sprintf(line4 + PRINTWIDTH * depth, "    ");
	printf("%s\n%s\n%s\n%s\n", line1, line2, line3, line4);
    }
}

void
pretty_print_trie()
{
    int i;

    pretty_print_trie_here(trie_root, 0);

    /* fix up all refcounts */
    for (i = 0; i < trie_size; i++) {
	if (trie[i].refcount < 0)
	    trie[i].refcount = -trie[i].refcount;
    }

}

/*
 * A small demonstration:
 * - read words from stdin
 * - store and compress them
 * - regurgitate them to stdout, so you can verify they are ok
 * - create a binary dump of the trie.
 */
int
main(int argc, char **argv)
{
    int nodes_left;
#if DEBUG
    setlinebuf(stdout);
    setlinebuf(stderr);
#endif

    trie_init(INITIAL_TRIESIZE);
    trie_input_size = trie_fill(stdin);
#if DEBUG
    pretty_print_trie();
#endif
    nodes_left = trie_compress();
#if DEBUG
    pretty_print_trie();
#endif
    //trie_dump_text();
#if HISTOGRAM
    {
	int i;
	int total = 0;

	fprintf(stderr, "distance    #nodes duplicate found\n");
	for (i = 0; i < DUPLICATE_SEARCH_DIST; i++) {
	    fprintf(stderr, "%4d: %9d\n", i, search_back_distance[i]);
	    total += search_back_distance[i];
	}
	total -= search_back_distance[0] +
	         search_back_distance[DUPLICATE_SEARCH_DIST-1];
	fprintf(stderr, "found %d duplicate at some distance\n", total);
    }
#if DUPLICATE_SEARCH_HASH
    {
	int i;

	fprintf(stderr, "hashval	#times updated\n");
	for (i = 0; i < DUPLICATE_SEARCH_HASH; i++) {
	    fprintf(stderr, "%4d: %9d\n", i, duplicate_search_hash_usage[i]);
	}
    }
#endif	/* DUPLICATE_SEARCH_HASH */
#endif	/* HISTOGRAM */
#if EXTRA_HISTOGRAM
    fprintf(stderr, "missed %d duplicate nodes\n", could_have_found);
#endif
    trie_dump_binary("bindump", nodes_left);

    return 0;
}
