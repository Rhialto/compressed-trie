/*
 * readtrie.c
 * Copyright 2005 Olaf Seibert.
 * Undump a trie and store it efficiently in memory.
 *
 */

#include <stdlib.h>
#include <stdio.h>
#include <string.h>
#include <errno.h>
#include <assert.h>
#include <ctype.h>

#include "trie.h"

/*
 * I (plan to) show two implementations for indicating
 * the end of the sibling list:
 *
 * - EOL: the last node is marked with a bit in the last node's child
 *        index
 * - COUNT: every reference to a node includes in the high bits the number
 *        of siblings that it has.
 *        This limits the number of siblings to about 256. If there are
 *        more, a continuation node is needed, just like with shared
 *        siblings.
 */

struct trie {
    int		size;
    int		next;
    int		root;
    Char       *character;
    TriePos    *child;
};

#if USE_EOL

#define	NO_SIBLING		(1L << 31)
#define CHILD_MASK		(~NO_SIBLING)
#define get_child(n)		(trie.child[n] & CHILD_MASK)
#define has_no_sibling(n)	(trie.child[n] & NO_SIBLING)
#define sibling_is_shared(n)	(trie.character[n] == CHAR_SHAREPTR)
#define sibling_of_ch_is_shared(ch) ((ch) == CHAR_SHAREPTR)
#define get_shared_sibling(n)	get_child(n)
#define get_unshared_sibling(n)	((n) + 1)
#define get_sibling(n)		\
		(has_no_sibling(n)      ? 0 : \
		  (sibling_is_shared(n) ? get_shared_sibling(n) : \
		  get_unshared_sibling(n)))

/*
 * The conditions has_no_sibling(n) and sibling_is_shared(n) can't logically
 * occur at the same time.
 *
 * Alternatively, the share condition can be indicated in the
 * child index (using bit 30). In that case character will be free for
 * other use. I don't know if that is useful for something. In that case,
 * sibling_of_ch_is_shared() would go away.
 */

#endif
#if USE_COUNT

#define CHILD_MASK		0x00FFFFFF
#define get_child(n)		(trie.child[n] & CHILD_MASK)
#define get_count(n)		(trie.child[n] >> 24)
#define has_no_sibling(n)	xxx
#define sibling_is_shared(n)	(trie.character[n] == CHAR_SHAREPTR)
#define sibling_of_ch_is_shared(ch) ((ch) == CHAR_SHAREPTR)
#define get_shared_sibling(n)	get_child(n)
#define get_unshared_sibling(n)	((n) + 1)
#define get_sibling(n)		\
		(has_no_sibling(n)      ? 0 : \
		  (sibling_is_shared(n) ? get_shared_sibling(n) : \
		  get_unshared_sibling(n)))
#endif

static struct trie trie;

int
trie_init(int size)
{
    trie.size = size;
    trie.character = calloc(size, sizeof(Char));
    trie.child =  calloc(size, sizeof(TriePos));
    trie.next = 0;

    trie.root = trie.next++;
    trie.character[trie.root] = '^';

    return size;
}

FILE *bindump;

static
int
undumpchar()
{
    unsigned char c;
    int result = 0;
    c = getc(bindump); result = c;

    return result;
}

static
int
undumpint()
{
    unsigned char c;
    int result = 0;
    c = getc(bindump); result = c;
    c = getc(bindump); result <<= 8; result |= c;
    c = getc(bindump); result <<= 8; result |= c;

    return result;
}

static
int
undumptriepos()
{
    unsigned char c;
    int result = 0;
    c = getc(bindump); result = c;
    c = getc(bindump); result <<= 8; result |= c;
    c = getc(bindump); result <<= 8; result |= c;

    return result;
}

static
TriePos
trie_undump_binary_rec(int nsib)
{
    Char c = undumpchar();

    if (c == CHAR_NULLPTR) {
	return 0;
    } else {
	TriePos here = trie.next;

	if (c == CHAR_SHAREPTR) {
	    TriePos there = undumptriepos();

	    if (nsib > 0) {
		trie.character[here] = CHAR_SHAREPTR;
		trie.child[here] = there;
		trie.next++;

		return here;
	    } else
		return there;
	} else {
	    /* a normal node: allocate room and store the character */
	    trie.character[here] = c;
	    trie.next++;

	    /* undump the sibling */
	    TriePos sibling = trie_undump_binary_rec(nsib + 1);
	    assert(sibling == 0 || sibling == here + 1);
	    if (sibling == 0) {
		trie.child[here] = NO_SIBLING;
	    }
	    /* undump the child */
	    if (c == CHAR_EOS) {
		/* except that an endmarker does not have a child */
	    } else {
		trie.child[here] |= trie_undump_binary_rec(0);
	    }

	    return here;
	}
    }
}

TriePos
trie_undump_binary(char *filename)
{
    int size;

    bindump = fopen(filename, "r");
    if (bindump) {
	size = undumpint();
	trie_init(size);
	trie.child[trie.root] = trie_undump_binary_rec(0);

	fclose(bindump);
	bindump = NULL;

	return trie.root;
    }

    fprintf(stderr, "Can't open bindump\n");
    return 0;
}

static Char accumulator[LINESIZE];

/*
 * The loop can be made slightly more efficient by using the
 * structural knowledge which is hidden in get_sibling(), since
 * we're looking at trie.character[here] anyway.
 * But I've written a more readable version.
 */
static
void
trie_dump_text_here(TriePos here, int depth)
{
    Char c;

    accumulator[depth] = c = trie.character[here];

    if (c == CHAR_EOS) {
	printf("%s\n", (char *)&accumulator[1]);
    } else {
	TriePos child = get_child(here);

	while (child != 0) {
	    if (trie.character[here] != CHAR_SHAREPTR) {
		trie_dump_text_here(child, depth + 1);
	    }
	    child = get_sibling(child);
	}
    }
}

void
trie_dump_text()
{
    trie_dump_text_here(trie.root, 0);
}

#define max(a,b)	((a) > (b)? (a) : (b))

/*
 * This is the function we're doing it all for: the word lookup function.
 * It matches a word with the dictionary, and does it slightly fuzzily: it
 * allows uppercase in the text when the dictionary has lowercase.  Adding
 * other fuzziness increases complexity, but a function that finds words
 * that are similar to one from the text is relatively simple.
 *
 * You can store UTF-8 sequences as bytes in the trie, but then you lose
 * fuzzy matching of any kind, or at least it makes it much more complex.
 * Unicode is a lot easier if Char is wchar_t (or whatever).
 */
int
trie_check_word_here(TriePos here, Char *word)
{
    Char ch = word[0];
    Char ch2 = tolower(ch);
    Char limit = max(ch, ch2);	/* for Latin-1, max(ch, ch2) == ch2 */

    here = get_child(here);

    while (here != 0) {
	Char chh = trie.character[here];

	if (chh == ch || chh == ch2) {
	    int found;
	    if (ch == '\0') {
		return 1;
	    }
	    found = trie_check_word_here(here, word + 1);
	    if (found || ch == ch2)
		return found;
	    /*
	     * If we find ch, recurse, and don't match after all,
	     * go on for ch2.
	     */
	}

	/*
	 * Go to the next sibling if there is one that might match,
	 * or get out of the loop. Assumes that limit > CHAR_SHAREPTR.
	 */
	if (chh > limit) {
	    break;			/* past a possible match */
	} else if (has_no_sibling(here)) {
	    break;			/* nothing left to match */
	} else if (sibling_of_ch_is_shared(chh)) {
	    here = get_shared_sibling(here);
	} else {
	    here = get_unshared_sibling(here);
	}
    }

    return 0;
}

int
trie_check_word(Char *word)
{
    return trie_check_word_here(trie.root, word);
}

int
test_words(FILE *f)
{
    char line[LINESIZE];
    int found;
    int errors = 0;

    while (fgets(line, LINESIZE, f)) {
	int len = strlen(line);
	if (len > 0 && line[len-1] == '\n')
	    line[--len] = '\0';
	found = trie_check_word((Char *)line);	/* XXX needs conversion of the string */
	if (!found) {
	    fprintf(stderr, "not found: '%s'\n", line);
	    errors++;
	}
	if (len > 3) {
	    line[3] = toupper(line[3]);
	    found = trie_check_word((Char *)line);	/* XXX needs conversion of the string */
	    if (!found) {
		fprintf(stderr, "not found: '%s'\n", line);
		errors++;
	    }
	}
	line[--len] = '\0';
	found = trie_check_word((Char *)line);	/* XXX needs conversion of the string */
	if (found) {
	    fprintf(stderr, "    found: '%s'\n", line);
	    errors++;
	}
    }

    return errors;
}

int
main(int argc, char **argv)
{
    trie_undump_binary("bindump");
    trie_dump_text();
    test_words(stdin);

    /* now test a bunch of words */
    fprintf(stderr, "test 1a: %d\n", trie_check_word("lymphagogue"));
    fprintf(stderr, "test 1A: %d\n", trie_check_word("lYmphAgogUe"));
    fprintf(stderr, "test 2: %d\n", trie_check_word("lympxagogue"));

    return 0;
}
