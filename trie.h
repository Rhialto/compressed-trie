/*
 * trie.h
 * Copyright 2005 Olaf Seibert.
 * Undump a trie and store it efficiently in memory.
 *
 */

typedef unsigned char Char;
typedef int TriePos;

/*
 * I show two implementations for indicating the end of the sibling
 * list:
 * - EOL: the last node is marked with a bit in the last node's child
 *        index
 * - COUNT: every reference to a node includes in the high bits the number
 *        of siblings that it has.
 *        This limits the number of siblings to about 256. If there are
 *        more, a continuation node is needed, just like with shared
 *        siblings.
 */

#define USE_EOL			1
#define USE_COUNT		0

/* These 3 values cannot be stored as part of a word: */
#define CHAR_EOS	0	/* end marker of a word in the trie [1] */
#define CHAR_SHAREPTR	1	/* pointer sharing in binary dump   [2] */ 
#define CHAR_NULLPTR	2	/* NULL pointer in binary dump      [3] */

/*
 * [1] must be 0 because it is the same as a C string terminator.
 * [2] must be lower than the lexicographically first character in the trie,
 *     i.e. < 32. trie_check_word_here() as written assumes this.
 * [3] may be an arbitrary value, but if Chars are written in the dump file
 *     in UTF-8 it would be better if it were < 128.
 */

/*
 * The maximum size of a word; only for practical reasons such
 * as buffer size.
 */
#define LINESIZE	256
